
# coding: utf-8

# In[1]:


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import tensorflow as tf


import gzip
import os
import sys

import tensorflow.python.platform

import numpy as np
from six.moves import urllib
from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf
import itertools


# In[2]:


IMAGE_SIZE = 28
NUM_CHANNELS = 1
PIXEL_DEPTH = 255
NUM_LABELS = 2
VALIDATION_SIZE = 5000  # Size of the validation set.
SEED = 66478  # Set to None for random seed.
BATCH_SIZE = 64
NUM_EPOCHS = 2


# In[3]:


import cv2                 # working with, mainly resizing, images
import numpy as np         # dealing with arrays
import os                  # dealing with directories
from random import shuffle # mixing up or currently ordered data that might lead our network astray in training.
from tqdm import tqdm      # a nice pretty percentage bar for tasks. Thanks to viewer Daniel B�hler for this suggestion

TRAIN_DIR = '/home/lt106/train'
TEST_DIR = '/home/lt106/test'
IMG_SIZE = 28
LR = 1e-3

MODEL_NAME = 'dogsvscats-{}-{}.model'.format(LR, '2conv-basic') # just so we remember which saved model is which, sizes must match


# In[4]:


def label_img(img):
    word_label = img.split('.')[-3]
    # conversion to one-hot array [cat,dog]
    #                            [much cat, no dog]
    if word_label == 'cat': return [1,0]
    #                             [no cat, very doggo]
    elif word_label == 'dog': return [0,1]


# In[5]:


def create_train_data():
    training_data = []
    for img in tqdm(os.listdir(TRAIN_DIR)):
        label = label_img(img)
        path = os.path.join(TRAIN_DIR,img)
        img = cv2.imread(path,cv2.IMREAD_GRAYSCALE)
        img = cv2.resize(img, (IMG_SIZE,IMG_SIZE))
        training_data.append([np.array(img),np.array(label)])
    shuffle(training_data)
    np.save('train_data.npy', training_data)
    return training_data


# In[6]:


def process_test_data():
    testing_data = []
    for img in tqdm(os.listdir(TEST_DIR)):
        path = os.path.join(TEST_DIR,img)
        img_num = img.split('.')[0]
        img = cv2.imread(path,cv2.IMREAD_GRAYSCALE)
        img = cv2.resize(img, (IMG_SIZE,IMG_SIZE))
        testing_data.append([np.array(img), img_num])
        print(img_num)
    shuffle(testing_data)
    np.save('test_data.npy', testing_data)
    return testing_data


# In[7]:


training_data_all = create_train_data()


# In[8]:


# test_data_all = process_test_data()


# In[9]:


training_data_all = np.array(training_data_all)


# In[10]:


training_data_all[0]


# In[11]:


training_data_all.shape


# In[12]:


train = training_data_all[:-5000]
test = training_data_all[-5000:]


# In[13]:


train


# In[14]:


X = (np.array([i[0] for i in train]).reshape(-1,IMG_SIZE,IMG_SIZE,1)).astype(np.float32)
Y = [i[1] for i in train]


# In[15]:


# X = X.astype(np.float32)


# In[16]:


Y


# In[17]:


X


# In[18]:


test_x = np.array([i[0] for i in test]).reshape(-1,IMG_SIZE,IMG_SIZE,1)
test_y = [i[1] for i in test]


# In[19]:


validation_data = X[:VALIDATION_SIZE]
validation_labels = Y[:VALIDATION_SIZE]
train_data = X[VALIDATION_SIZE:]
train_labels = Y[VALIDATION_SIZE:]
num_epochs = NUM_EPOCHS
train_size = train_data.shape[0]


# In[20]:


type(train_labels)


# In[21]:


#!/usr/bin/python



class ConvNet(object):

    pass


def create_graph(
    base_learning_rate=0.01,
    decay_rate=0.95,
    conv1_size=32,
    conv2_size=64,
    fc1_size=512,
    ):
    """ A TF graph that can be run on mini batches of data.
  """

  # This is where training samples and labels are fed to the graph.
  # These placeholder nodes will be fed a batch of training data at each
  # training step using the {feed_dict} argument to the Run() call below.

    train_data_node = tf.placeholder(tf.float32, shape=(BATCH_SIZE,
            IMG_SIZE,IMG_SIZE,1))
    train_labels_node = tf.placeholder(tf.float32, shape=(BATCH_SIZE,
            NUM_LABELS))

  # For the validation and test data, we'll just hold the entire dataset in
  # one constant node.

    validation_data_node = tf.constant(validation_data)
    test_data_node = tf.constant(validation_data)

  # The variables below hold all the trainable weights. They are passed an
  # initial value which will be assigned when when we call:
  # {tf.initialize_all_variables().run()}

    conv1_weights = tf.Variable(tf.truncated_normal([5, 5,
                                NUM_CHANNELS, conv1_size], stddev=0.1,
                                seed=SEED))  # 5x5 filter, depth 32.
    conv1_biases = tf.Variable(tf.zeros([conv1_size]))
    conv2_weights = tf.Variable(tf.truncated_normal([5, 5, conv1_size,
                                conv2_size], stddev=0.1, seed=SEED))
    conv2_biases = tf.Variable(tf.constant(0.1, shape=[conv2_size]))
    fc1_weights = tf.Variable(tf.truncated_normal([IMAGE_SIZE // 4
                              * IMAGE_SIZE // 4 * conv2_size,
                              fc1_size], stddev=0.1, seed=SEED))  # fully connected, depth 512.
    fc1_biases = tf.Variable(tf.constant(0.1, shape=[fc1_size]))
    fc2_weights = tf.Variable(tf.truncated_normal([fc1_size,
                              NUM_LABELS], stddev=0.1, seed=SEED))
    fc2_biases = tf.Variable(tf.constant(0.1, shape=[NUM_LABELS]))

  # We will replicate the model structure for the training subgraph, as well
  # as the evaluation subgraphs, while sharing the trainable parameters.

    def model(data, train=False):
        """The Model definition."""

    # 2D convolution, with 'SAME' padding (i.e. the output feature map has
    # the same size as the input). Note that {strides} is a 4D array whose
    # shape matches the data layout: [image index, y, x, depth].

        conv = tf.nn.conv2d(data, conv1_weights, strides=[1, 1, 1, 1],
                            padding='SAME')

    # Bias and rectified linear non-linearity.

        relu = tf.nn.relu(tf.nn.bias_add(conv, conv1_biases))

    # Max pooling. The kernel size spec {ksize} also follows the layout of
    # the data. Here we have a pooling window of 2, and a stride of 2.

        pool = tf.nn.max_pool(relu, ksize=[1, 2, 2, 1], strides=[1, 2,
                              2, 1], padding='SAME')
        conv = tf.nn.conv2d(pool, conv2_weights, strides=[1, 1, 1, 1],
                            padding='SAME')
        relu = tf.nn.relu(tf.nn.bias_add(conv, conv2_biases))
        pool = tf.nn.max_pool(relu, ksize=[1, 2, 2, 1], strides=[1, 2,
                              2, 1], padding='SAME')

    # Reshape the feature map cuboid into a 2D matrix to feed it to the
    # fully connected layers.

        pool_shape = pool.get_shape().as_list()
        reshape = tf.reshape(pool, [pool_shape[0], pool_shape[1]
                             * pool_shape[2] * pool_shape[3]])

    # Fully connected layer. Note that the '+' operation automatically
    # broadcasts the biases.

        hidden = tf.nn.relu(tf.matmul(reshape, fc1_weights)
                            + fc1_biases)

    # Add a 50% dropout during training only. Dropout also scales
    # activations such that no rescaling is needed at evaluation time.

        if train:
            hidden = tf.nn.dropout(hidden, 0.5, seed=SEED)
        return tf.matmul(hidden, fc2_weights) + fc2_biases

  # Training computation: logits + cross-entropy loss.

    logits = model(train_data_node, True)
    loss =         tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits,
                       labels=train_labels_node))

  # L2 regularization for the fully connected parameters.

    regularizers = tf.nn.l2_loss(fc1_weights)         + tf.nn.l2_loss(fc1_biases) + tf.nn.l2_loss(fc2_weights)         + tf.nn.l2_loss(fc2_biases)

  # Add the regularization term to the loss.

    loss += 5e-4 * regularizers

  # Optimizer: set up a variable that's incremented once per batch and
  # controls the learning rate decay.

    batch = tf.Variable(0)

  # Decay once per epoch, using an exponential schedule starting at 0.01.

    learning_rate = tf.train.exponential_decay(base_learning_rate,
            batch * BATCH_SIZE, train_size, decay_rate, staircase=True)  # Base learning rate.

                                                                         # Current index into the dataset.
                                                                         # Decay step.
                                                                         # Decay rate.

  # Use simple momentum for the optimization.

    optimizer = tf.train.MomentumOptimizer(learning_rate,
            0.9).minimize(loss, global_step=batch)

  # Predictions for the minibatch, validation set and test set.

    train_prediction = tf.nn.softmax(logits)

  # We'll compute them only once in a while by calling their {eval()} method.

    validation_prediction = tf.nn.softmax(model(validation_data_node))
    test_prediction = tf.nn.softmax(model(test_data_node))

    res = ConvNet()
    res.train_prediction = train_prediction
    res.optimizer = optimizer
    res.loss = loss
    res.learning_rate = learning_rate
    res.validation_prediction = validation_prediction
    res.test_prediction = test_prediction
    res.train_data_node = train_data_node
    res.train_labels_node = train_labels_node
    return res



			


# In[22]:


train_data_bc = sc.broadcast(train_data)
train_labels_bc = sc.broadcast(train_labels)

def run(base_learning_rate, decay_rate, fc1_size):
    train_data = train_data_bc.value
    train_labels = train_labels_bc.value
    res = {}
    res['base_learning_rate'] = base_learning_rate
    res['decay_rate'] = decay_rate
    res['fc1_size'] = fc1_size
    res['minibatch_loss'] = 100.0
    res['test_error'] = 100.0
    res['validation_error'] = 100.0
    # Training may fail to converge, or even diverge; guard against that.
    try:
    # Create a local session to run this computation.
        with tf.Session() as s:
            s.run(tf.global_variables_initializer())
          # Create the computation graph
            graph = create_graph(base_learning_rate, decay_rate, fc1_size=fc1_size)
            # Run all the initializers to prepare the trainable parameters.
            tf.initialize_all_variables().run()
            # Loop through training steps.
            for step in xrange(num_epochs * train_size // BATCH_SIZE):
                # Compute the offset of the current minibatch in the data.
                # Note that we could use better randomization across epochs.
                offset = (step * BATCH_SIZE) % (train_size - BATCH_SIZE)
                batch_data = train_data[offset:(offset + BATCH_SIZE), :, :, :]
                batch_labels = train_labels[offset:(offset + BATCH_SIZE)]
                feed_dict = {graph.train_data_node: batch_data,
                         graph.train_labels_node: batch_labels}
            # Run the graph and fetch some of the nodes.
                _, l, lr, predictions = s.run(
                    [graph.optimizer, graph.loss, graph.learning_rate, graph.train_prediction],
                    feed_dict=feed_dict)
                res['minibatch_loss'] = l
            res['test_error'] = error_rate(graph.test_prediction.eval(), validation_labels)
#             res['validation_error'] = error_rate(graph.test_prediction.eval(), val)
            res['validation_error'] = error_rate(graph.validation_prediction.eval(), validation_labels)
            return res
    except Exception as e:
        print("Exception", e)
        pass
    return res


# In[23]:

def error_rate(predictions, labels):
  """Return the error rate based on dense predictions and 1-hot labels."""
  return 100.0 - (
      100.0 *
      np.sum(np.argmax(predictions, 1) == np.argmax(labels, 1)) /
      predictions.shape[0])
      
      
base_learning_rates = [float(x) for x in np.logspace(-3, -1, num=10, base=10.0)]
decay_rates = [0.95]
# fc1_sizes = [64, 128, 256, 512, 1024]
fc1_sizes = [64,128]
all_experiments = list(itertools.product(base_learning_rates, decay_rates, fc1_sizes))
print(len(all_experiments))


# In[24]:


all_exps_rdd = sc.parallelize(all_experiments, numSlices=len(all_experiments)) 


# In[25]:


num_nodes = 4
n = max(2, int(len(all_experiments) // num_nodes))
grouped_experiments = [all_experiments[i:i+n] for i in range(0, len(all_experiments), n)]
all_exps_rdd = sc.parallelize(grouped_experiments, numSlices=len(grouped_experiments))
results = all_exps_rdd.flatMap(lambda z: [run(*y) for y in z]).collect()


# In[26]:


import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
np.random.seed(sum(map(ord, "aesthetics")))
import pandas as pd


# In[27]:


import pandas as pd
df = pd.DataFrame(results)
df

